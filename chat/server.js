var express = require('express'),
    app = express.createServer(),
	io = require('socket.io').listen(app),
	irc = require('irc');

app.configure(function() {
    app.use(app.router);
    app.use(express.static(__dirname + '/public'));
});

app.configure('development', function() {
    app.listen(3000);
});

app.configure('production', function() {
    app.listen(12445);
});

app.get('/', function(req, res, next) {
    next();
});

console.log('Servidor conectado na porta %s', app.address().port);


// Socket.IO
io.sockets.on('connection', function(socket) {

    // Eventos 
    var events = {
        'join': ['channel', 'nick'],
        'part': ['channel', 'nick'],
        'topic': ['channel', 'topic', 'nick'],
        'nick': ['oldNick', 'newNick', 'channels'],
        'names': ['channel', 'nicks'],
        'message': ['from', 'to', 'text'],
        'pm': ['nick', 'text'],
        'motd': ['motd'],
        'error': ['message']
    };

    socket.on('connect', function(data) {
        var client = new irc.Client(data.server, data.nick, {
            debug: true,
            showErrors: true,
            channels: data.channels
        });

        // Socket de eventos 
        socket.on('join', function(name) { client.join(name); });
        socket.on('part', function(name) { client.part(name); });
        socket.on('say', function(data) { client.say(data.target, data.message); });
        socket.on('command', function(text) { console.log(text); client.send(text); });
        socket.on('disconnect', function() { client.disconnect(); });


        // Adicionar um listener no cliente para os nomes de argumento e de evento fornecidos
        var activateListener = function(event, argNames) {
            client.addListener(event, function() {
                console.log('Event ' + event + ' sent');
                // Associar nomes especificos com argumentos callback
                var callbackArgs = arguments;
                args = {};
                argNames.forEach(function(arg, index) {
                    args[arg] = callbackArgs[index];
                });
                console.log(args);
                socket.emit(event, args);
            });
        };

        for (var event in events) { activateListener(event, events[event]); }
        console.log('Iniciando o cliente IRC.')
    });
});
